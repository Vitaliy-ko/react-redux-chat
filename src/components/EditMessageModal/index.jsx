import React from 'react';
import styles from './styles.module.scss';

export const EditMessageModal = ({
  editMessage,
  hideModal,
  editingMessage,
  editingMessageText,
  setEditingMessageText,
}) => {
  const saveMessageHandler = () => {
    editMessage(editingMessageText);
    setEditingMessageText('');
  };

  return (
    <div
      className={['modal', styles.modal].join(' ')}
      tabIndex="-1"
      role="dialog"
    >
      <div className={['modal-dialog', styles.modalDialog].join(' ')}>
        <div className="modal-content">
          <div className="modal-header">
            <h5 className="modal-title">Edit message</h5>
            <button
              onClick={hideModal}
              type="button"
              className="close"
              data-dismiss="modal"
              aria-label="Close"
            >
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div className="modal-body">
            <textarea
              className={styles.message}
              value={editingMessageText || editingMessage?.text}
              onChange={(e) => setEditingMessageText(e.target.value)}
            ></textarea>
          </div>
          <div className="modal-footer">
            <button
              onClick={hideModal}
              type="button"
              className="btn btn-secondary"
              data-dismiss="modal"
            >
              Close
            </button>
            <button
              type="button"
              className="btn btn-warning"
              onClick={() => saveMessageHandler(editingMessageText)}
            >
              Save changes
            </button>
          </div>
        </div>
      </div>
    </div>
  );
};
