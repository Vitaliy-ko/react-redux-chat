import { createStore, applyMiddleware, compose, combineReducers } from 'redux';
import thunk from 'redux-thunk';
// import { composeWithDevTools } from 'redux-devtools-extension';

import messagesReducer from './containers/Chat/reducers';

const initialState = {};
const middlewares = [thunk];

const composedEnhancers = compose(applyMiddleware(...middlewares));

const reducers = {
  messages: messagesReducer,
};

const rootReducer = combineReducers({
  ...reducers,
});

const store = createStore(rootReducer, initialState, composedEnhancers);

export default store;
