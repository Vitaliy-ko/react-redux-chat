import { v4 as uuidv4 } from 'uuid';

import {
  GET_MESSAGES_SUCCESS,
  SEND_MESSAGE,
  EDIT_MESSAGE,
  DELETE_MESSAGE,
  LIKE_MESSAGE,
  SET_EDITING_MESSAGE,
  HIDE_MODAL,
} from './actionsTypes';
import { getMessagesService } from '../service';
import WillSmith from './../../../assets/images/WillSmith.jpg';

const currentUserId = 'userId';

const messageTemplate = {
  id: '',
  text: '',
  user: 'Will Smith',
  avatar: WillSmith,
  userId: currentUserId,
  editedAt: '',
  createdAt: '',
  like: new Map(),
};

const getMessagesSuccess = (messages) => ({
  type: GET_MESSAGES_SUCCESS,
  messages,
});

const sendMessageSuccess = (messages) => ({
  type: SEND_MESSAGE,
  messages,
});

export const getMessages = () => async (dispatch) => {
  const messages = await getMessagesService() || [];
  dispatch(getMessagesSuccess(messages));
};

export const sendMessage = (text) => (dispatch, getState) => {
  const { messages } = getState().messages;

  const message = {
    ...messageTemplate,
    id: uuidv4(),
    text,
    createdAt: new Date(),
  };
  const updatedMessages = [...messages, message];

  dispatch(sendMessageSuccess(updatedMessages));
};

const editMessageSuccess = (messages) => ({
  type: EDIT_MESSAGE,
  messages,
});

const deleteMessageSuccess = (messages) => ({
  type: DELETE_MESSAGE,
  messages,
});

const likeMessageSuccess = (messages) => ({
  type: LIKE_MESSAGE,
  messages,
});

export const setEditingMessageSuccess = (message) => ({
  type: SET_EDITING_MESSAGE,
  message,
});

export const hideModal = () => {
  return {
    type: HIDE_MODAL,
  };
};

export const editMessage = (text) => (dispatch, getState) => {
  const editedAt = new Date();

  const { messages, editingMessage } = getState().messages;
  const updatedMessages = [...messages];
  const { id: messageId } = editingMessage;

  const editedMessage = { ...editingMessage, text, editedAt };

  const editedMessageIndex = messages.findIndex(
    (message) => message.id === messageId
  );

  updatedMessages.splice(editedMessageIndex, 1, editedMessage);

  dispatch(editMessageSuccess(updatedMessages));
};

export const deleteMessage = (messageId) => (dispatch, getState) => {
  const { messages } = getState().messages;
  const updatedMessages = [...messages];
  const deleteMessageIndex = updatedMessages.findIndex(
    (message) => message.id === messageId
  );
  updatedMessages.splice(deleteMessageIndex, 1);

  dispatch(deleteMessageSuccess(updatedMessages));
};

export const likeMessage = (messageId, currentUserId) => (
  dispatch,
  getState
) => {
  const { messages } = getState().messages;
  const updatedMessages = [...messages];

  const likeMessageIndex = updatedMessages.findIndex(
    (message) => message.id === messageId
  );

  const editingMessage = updatedMessages[likeMessageIndex];

  const isMessageLiked =
    editingMessage.like && editingMessage?.like.has(currentUserId);

  if (isMessageLiked) {
    editingMessage.like.delete(currentUserId);
  } else {
    editingMessage.like = new Map();
    editingMessage.like.set(currentUserId, true);
  }

  updatedMessages.splice(likeMessageIndex, 1, editingMessage);
  dispatch(likeMessageSuccess(updatedMessages));
};

export const setEditingMessage = (message) => (dispatch, getState) => {
  let resultMessage = message;

  if (!message) {
    const { messages } = getState().messages;
    resultMessage = messages[messages.length - 1];

    if (resultMessage.userId !== currentUserId) {
      return;
    }
  }

  dispatch(setEditingMessageSuccess(resultMessage));
};
