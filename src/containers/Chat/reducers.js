import {
  GET_MESSAGES_SUCCESS,
  SEND_MESSAGE,
  EDIT_MESSAGE,
  DELETE_MESSAGE,
  LIKE_MESSAGE,
  SET_EDITING_MESSAGE,
  HIDE_MODAL,
} from './actions/actionsTypes';

const initialState = {
  isLoaded: false,
  messages: [],
  editingMessage: null,
  isEditModalShown: false,
};

const setMessages = (state, { messages }) => ({
  ...state,
  isLoaded: true,
  messages,
});

const updateMessages = (state, { messages }) => ({
  ...state,
  messages,
});

const editMessage = (state, { messages }) => ({
  ...state,
  messages,
  editingMessage: null,
  isEditModalShown: false
});

const deleteMessage = (state, { messages }) => ({
  ...state,
  messages,
});

const setEditingMessage = (state, { message }) => ({
  ...state,
  editingMessage: message,
  isEditModalShown: true,
});

const hideModal = (state) => ({
  ...state,
  isEditModalShown: false,
});


export default (state = initialState, action) => {
  switch (action.type) {
    case GET_MESSAGES_SUCCESS:
      return setMessages(state, action);
    case SEND_MESSAGE:
      return updateMessages(state, action);
    case EDIT_MESSAGE:
      return editMessage(state, action);
    case DELETE_MESSAGE:
      return deleteMessage(state, action);
    case SET_EDITING_MESSAGE:
      return setEditingMessage(state, action);
    case LIKE_MESSAGE:
      return updateMessages(state, action);
    case HIDE_MODAL:
      return hideModal(state, action);
    default:
      return state;
  }
};
